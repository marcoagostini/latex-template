thesis:
	pdflatex -synctex=1 -interaction=nonstopmode -file-line-error main.tex
	makeglossaries main
	biber main
	pdflatex -synctex=1 -interaction=nonstopmode -file-line-error main.tex
	makeglossaries main
	pdflatex -synctex=1 -interaction=nonstopmode -file-line-error main.tex

clean:
	latexmk -c